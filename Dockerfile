FROM php:7.4-apache

WORKDIR /var/www/html

COPY . /var/www/html

EXPOSE 80

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

CMD ["apache2-foreground"]

# Ajout d'une instruction de débogage
RUN ls -l /usr/bin/curl
