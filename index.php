<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trombinoscope</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        /* Ajoutez cette règle pour changer la couleur du fond */
        .custom-background {
            background-color: #ffffff; /* Remplacez la couleur par celle que vous souhaitez */
        }
    </style>
</head>
<body class="container mt-5 custom-background">
    <h1 class="text-center">Trombinoscope</h1>

    <?php
    $file = 'employees.json';
    $employees = json_decode(file_get_contents($file), true);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Ajouter un nouvel employé
        $newEmployee = [
            'nom' => $_POST['nom'],
            'prenom' => $_POST['prenom'],
            'age' => $_POST['age'],
            'adresse_mail' => $_POST['adresse_mail'],
            'metier' => $_POST['metier'],
            'image' => $_POST['image']
        ];
        $employees[] = $newEmployee;
        file_put_contents($file, json_encode($employees, JSON_PRETTY_PRINT));
    }
    ?>

        <div class="form-row" id="form">
            <?php foreach ($employees as $employee): ?>
                <div class="col-md-4 mt-2">
                    <div class="card">
                        <div class="card-header" id="header_<?php echo md5($employee['prenom'] . $employee['nom']); ?>">
                            <?php
                            echo '<img src="data:image/png;base64,' . $employee['image'] . '" class="card-img-top" alt="' . $employee['prenom'] . ' ' . $employee['nom'] . '">';
                            ?>
                            <center>
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?php echo md5($employee['prenom'] . $employee['nom']); ?>" aria-expanded="false" aria-controls="collapse_<?php echo md5($employee['prenom'] . $employee['nom']); ?>">
                                <?php echo $employee['prenom'] ." ". $employee['nom'] ?>
                            </button>
                            </center>
                        </div>
                        <div id="collapse_<?php echo md5($employee['prenom'] . $employee['nom']); ?>" class="collapse" aria-labelledby="header_<?php echo md5($employee['prenom'] . $employee['nom']); ?>" data-parent="#form">
                            <div class="card-body">
                                <center>
                                    <h5 class="card-title"><?php echo $employee['prenom'] . ' ' . $employee['nom']; ?></h5>
                                    <h5 class="card-title"><?php echo $employee['age']. ' ans'; ?></h5>
                                    <h5 class="card-title"><?php echo $employee['adresse_mail']; ?></h5>
                                    <h5 class="card-title"><?php echo $employee['metier']; ?></h5>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    <h2 class="mt-5">Ajouter un employé</h2>
    <form method="post">
        <!-- Votre formulaire ici -->
    </form>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
